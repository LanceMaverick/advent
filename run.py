from adv.logconf import configure_logging
from adv import create_app
configure_logging()
app = create_app()

if __name__ == "__main__":
    app.run()
