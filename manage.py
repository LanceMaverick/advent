from flask_script import Manager
from flask_migrate import MigrateCommand
from adv import create_app
from adv.models import *


manager = Manager(create_app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
