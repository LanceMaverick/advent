# Advent group event organiser
## Installation

Clone the repository:
```
git clone https://gitlab.com/LanceMaverick/advent.git
```

Install the requirements by doing the following from the root folder of the project:
```
pip install -r requirements.txt
```
Set up the configuration by renaming the config.py.example in the adv folder to config.py and define the salts and key

Create the database and tables by running:
```
python manage.py db init
python managy.py db migrate
python manage.py db upgrade
```
run the dev server with:
```
python run.py
```


