from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
from flask import current_app as app

from adv.models import Poll
from adv.forms import NewPollForm
from adv.database import db

bp = Blueprint('main', __name__)

@bp.route('/')
def index():
    polls = Poll.query.all()
    return render_template('main/index.html', polls=polls)

@bp.route('/new_poll', methods=('GET', 'POST'))
def new_poll():
    form = NewPollForm(request.form)
    if form.validate_on_submit():
        poll = Poll(
            begin = form.begin.data,
            end = form.end.data,
            location = form.location.data
        )
        db.session.add(poll)
        db.session.commit()
        return url_for('main.index')
    return render_template('main/new_poll.html', form=form)

        
