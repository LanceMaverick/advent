import functools
import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from flask_security import login_user, logout_user, current_user, login_required
from flask_security import SQLAlchemyUserDatastore, utils

from flask import current_app as app

from adv.auth import admin_required
from adv.models import User, Role
from adv.forms import NewUserForm, EditUserForm
from .database import db

bp = Blueprint('admin', __name__, url_prefix='/admin')

#TODO refactor for common functions
user_datastore = SQLAlchemyUserDatastore(db, User, Role)

@bp.route('/')
@admin_required
def administer():
    users = User.query.all()
    return render_template('admin/admin.html', users=users)


def role_tiers(tier):
    if tier == '':
        return []
    tiers = dict(
        member = ['member'],
        admin = ['member', 'admin']
    )
    try:
        return tiers[tier]
    except KeyError:
        app.logger.error('role tier {} does not exist'.format(tier))

@bp.route('/new_user', methods=('GET', 'POST'))
@admin_required
def new_user():
    
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        tier = form.tier.data
        if not user_datastore.get_user(username):
            user = user_datastore.create_user(username=username, password=utils.hash_password(password))
            for role in role_tiers(tier):
                user_datastore.add_role_to_user(user, role)
            
            db.session.commit()
            flash('User "{}" created with roles: [{}]. Temporary password: {}'.format(
                username, 
                ', '.join([r.name for r in user.roles]),
                password
                ), 'success')
            return redirect(url_for('admin.administer'))
        else:
            flash('User "{}" already exists'.format(username), 'danger')

    return render_template('admin/user.html', form=form)

@bp.route('/edit_user', methods=('GET', 'POST'))
@admin_required
def edit_user():
    users = User.query.all()
    active_map = json.dumps({u.id:u.active for u in users})
    form = EditUserForm(request.form)
    form.user.choices =  [(u.id, u.username) for u in users]

    if form.validate_on_submit():
        user_id = form.user.data
        password = form.password.data
        tier = form.tier.data
        active = form.active.data

        user = User.query.filter_by(id=user_id).first()
        
        if password:
            user.set_password(password)
            user.confirmed_at = None

        new_roles = role_tiers(tier)
        if new_roles:
            user.roles = []
            for role in role_tiers(tier):
                user_datastore.add_role_to_user(user, role)
        
        user.active = active
        db.session.commit()
        flash('User "{}" with roles [{}] successfully edited'.format(
            ', '.join([r.name for r in user.roles]),
            user.username), 'success')
        if password:
            flash('Password has been temporarily reset to: {}'.format(password), 'success')
        return redirect(url_for('admin.administer'))

    return render_template('admin/user.html', form = form, active_map = active_map)