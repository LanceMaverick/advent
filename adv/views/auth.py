import functools
from datetime import datetime
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from flask_security import login_user, logout_user, current_user, login_required
from flask_security.forms import ChangePasswordForm
from flask import current_app as app

from adv.models import User
from adv.forms import LoginForm
from adv.database import db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register')
def register():
    return "Registering is handled manually by the admin."
#register view
#@bp.route('/register', methods=('GET', 'POST'))
#def register():
#    if request.method == 'POST':
#        username = request.form['username']
#        password = request.form['password']
#        #db = get_db()
#        error = None
#
#        if not username:
#            error = 'Username is required.'
#        elif not password:
#            error = 'Password is required.'

#        elif User.query.filter_by(username = username).first() is not None:
#            error = 'User {} is already registered.'.format(username)
#        if error is None:
#            user = User(username = username, password = generate_password_hash(password))
#            db.session.add(user)
#            db.session.commit()
#            return redirect(url_for('auth.login'))
#
#        flash(error)
#
#    return render_template('auth/register.html')

#login view
@bp.route('/login', methods=('GET', 'POST'))
def login():
    next_param = request.args.get('next')
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        remember = form.remember.data
        error = None
        user = User.query.filter_by(username=username).first()
        if user is None or not user.check_password(password):
            flash('Incorrect username or password.','danger')
            return redirect(url_for('auth.login', form=form, next=next_param))
        if user.active:
            login_user(user, remember=remember)
            user.last_login = datetime.now()
            db.session.commit()
        else:
            flash('This account has been suspended. Contact Wazzerbosh.','danger')
        #check if user has logged in before and changed temporary password.
        if not user.confirmed_at:
            flash('You have not changed your password from the temporary one. Please do so now.', 'warning')
            return redirect(url_for('auth.account'))
        elif next_param:
                return redirect(next_param)
        #    session.clear()
        #    session['user_id'] = user.id
        return redirect(url_for('index', error=error))

    return render_template('auth/login.html', form=form)

@login_required
@bp.route('/account', methods=('GET', 'POST'))
def account():
    user = current_user
    form = ChangePasswordForm(request.form)
    if request.method =='POST':
        if form.validate():
            password = form.new_password.data
            user.set_password(password)
            user.confirmed_at = datetime.now()
            db.session.commit()
            flash('Password updated!', 'success')
        else:
            flash('ERROR', 'danger')
            for e in form.password.errors:
                flash(e, 'danger')
    return render_template('auth/account.html', form=form)

@bp.before_app_request
def load_logged_in_user():
    g.user = current_user
    #user_id = session.get('user_id')
    #
    #if user_id is None:
    #    g.user = None
    #else:
    #    g.user = User.query.filter_by(id = user_id).first()
    #    """
    #    g.user = get_db().execute(
    #        'SELECT * FROM user WHERE id = ?', (user_id,)
    #    ).fetchone()"""

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

#login check decorator
#def login_required(view):
#    @functools.wraps(view)
#    def wrapped_view(**kwargs):
#        if not g.user.is_authenticated:
#            return redirect(url_for('auth.login'))
#        return view(**kwargs)
#    return wrapped_view

#officer auth decorator
def officer_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('officer'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view

def admin_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('admin'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view


@bp.route('/denied')
def denied():
    return render_template('auth/denied.html')
