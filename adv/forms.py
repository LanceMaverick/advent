from distutils.util import strtobool

from flask_wtf import FlaskForm
from wtforms.ext.sqlalchemy.orm import model_form
from wtforms import SubmitField, StringField, PasswordField, SelectField, SubmitField, TextAreaField, BooleanField, HiddenField, validators
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired

from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES

from adv import db

class field_helpers:
    def date_picker(label="Date", required=True):
        if required:
            return DateField(label, [validators.required()], format='%Y-%m-%d' )
        else:
            return DateField(label, [], format='%Y-%m-%d' )

# form to log in
class LoginForm(FlaskForm):
    username = StringField(u'Username', [validators.required(), validators.length(max=120)])
    password = PasswordField(u'Password', [validators.required()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

#admin form to create a new user
class NewUserForm(FlaskForm):
    username = StringField(u'Username', [validators.required(), validators.length(max=120)])
    password = StringField(u'Password', [validators.required()])
    tier = SelectField(u'Tier', choices=[('','No Change'), ('member', 'member'), ('admin', 'admin')])
    submit = SubmitField('Submit')

#admin form to edit a user
class EditUserForm(FlaskForm):
    user = SelectField('User', coerce=int)
    password = StringField(u'Password')
    tier = SelectField(u'Tier', choices=[('','No Change'), ('member', 'member'), ('admin', 'admin')])
    active = BooleanField('Active')
    submit = SubmitField('Submit')

#form to create a new poll
class NewPollForm(FlaskForm):
    begin = field_helpers.date_picker() # begin and end of time period (e.g a weekend)
    end = field_helpers.date_picker()
    location = StringField(u'Location', [validators.required(), validators.length(max=120)])
    submit = SubmitField('Create')

#form to create an event within a poll
class NewEventForm(FlaskForm):
    name = StringField(u'Location', [validators.required(), validators.length(max=80)])
    description = TextAreaField(u'Description (optional)', [validators.optional(), validators.length(max=1000)])
    date = field_helpers.date_picker()
    link = TextAreaField(u'URL (optional)', [validators.optional(), validators.length(max=1000)])
    submit = SubmitField('Create')