import os
from flask import Flask
from flask_assets import Environment, Bundle
from flask_cors import CORS
from flask_bootstrap import Bootstrap
#from flask_sqlalchemy import SQLAlchemy
from flask_scss import Scss
#from flask_alembic import Alembic
from flask_migrate import Migrate
from flask_login import LoginManager
from .database import db
from .config import Config

#test_config = ''
basedir = os.path.abspath(os.path.dirname(__file__))

def create_app(test_config=None):
# create and configure the app
    app = Flask(__name__, instance_relative_config=True, instance_path=os.path.join(basedir, 'static/instance/'))
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'


    assets = Environment(app)
    assets.url = app.static_url_path
    scss = Bundle(
        '_variables.scss',
        'app.scss',
        'icons.scss',
        'login.scss',
        'cookiebar.scss',
        filters='pyscss',
        output='all.css')
    assets.register('scss_all', scss)
    
    app.config.from_mapping(
        SECRET_KEY=Config.FLASK_KEY,
        UPLOAD_FOLDER = os.path.join(app.instance_path, 'images/uploads'),
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        SECURITY_PASSWORD_HASH = 'bcrypt',
        SECURITY_PASSWORD_SALT=Config.SALT,
        SECURITY_REMEMBER_SALT = Config.REMEMBER_SALT,
        SECURITY_USER_IDENTITY_ATTRIBUTES = 'username',
        SECURITY_LOGIN_URL = '/auth/login'
    )
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    Bootstrap(app)
#alembic.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
      # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path, exist_ok=True)
        os.makedirs(os.path.join(app.instance_path, '/images/uploads'), exist_ok=True)

    except OSError:
        pass

#db init
#manager = Manager(app)
#manager.add_command('db', MigrateCommand)
       
#setup instance folder for uploads

#auth bp register
    from .views import auth
    app.register_blueprint(auth.bp)
 
    from .views import main
    app.register_blueprint(main.bp)

#init db
    db.init_app(app)
    migrate = Migrate(app, db)
    from adv import models

#init login manager
    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'
    #test only TODO
    @login_manager.user_loader
    def load_user(user_id):
        return None

#set up flask security
    from flask_security import Security, SQLAlchemyUserDatastore
    user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
    security = Security()
    security.init_app(app, user_datastore)
    security.login_view = 'auth.login'
    


#global template functions
    def nl2br(s):
        if not s:
            return s
        else:
            return s.replace('\n', '<br>')

    app.jinja_env.globals.update(nl2br=nl2br)

    return app


    
if __name__ == '__main__':
    

    #run main app
    app = create_app()
    app.run()
