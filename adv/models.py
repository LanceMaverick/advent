from datetime import datetime
from .database import db
from flask_security.utils import hash_password, verify_and_update_password
from flask_security import UserMixin, RoleMixin, utils

#relationships table for user roles
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


#relationship table for votes from users
vote_user_table = db.Table('vote_user_table',
        db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
        db.Column('vote_id', db.Integer, db.ForeignKey('vote.id')),
        )

#relationship table for votes on events
vote_event_table = db.Table('vote_event_table',
        db.Column('event_id', db.Integer, db.ForeignKey('event.id')),
        db.Column('vote_id', db.Integer, db.ForeignKey('vote.id')),
        )

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False, index=True)
    password = db.Column(db.String(), nullable=False)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    last_login = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    votes = db.relationship('Vote', secondary = 'vote_user_table', backref = 'user_votes')
    

    def check_password(self, password):
        return verify_and_update_password(password, self)
    
    def set_password(self, password):
        self.password = hash_password(password)

    def __repr__(self):
        return '<User %r>' % self.username

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.Text(), nullable=True)
    date = db.Column(db.DateTime())
    link = db.Column(db.String(), unique=True, nullable=True)
    votes = db.relationship('Vote', secondary = 'vote_event_table', backref = 'event_votes')

#relationship table for events in poll
event_table = db.Table('event_table',
        db.Column('poll_id', db.Integer, db.ForeignKey('poll.id')),
        db.Column('event_id', db.Integer, db.ForeignKey('event.id')),
        )

class Poll(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    begin = db.Column(db.DateTime())
    end = db.Column(db.DateTime()) # begin and end of time period (e.g a weekend)
    location = db.Column(db.String(80))
    events = db.relationship('Event', secondary = event_table, backref = db.backref('events'))


class Vote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rank = db.Column(db.Integer, nullable=False)

    
    
